#Remove duplicate clients. assume, that a client is unambiguously identified by his birthday and the first and second names.
DELETE
FROM
    assume
WHERE
    RowID NOT IN (
        SELECT
            MIN(RowID)
        FROM
            assume
        GROUP BY
            birthday,
            firstName,
            secondName
    )
