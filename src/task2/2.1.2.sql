# Convert data from easycar-clients.csv to sql format and save it to easycar-clients.sql.
create table easycar_clients(
	firstName     varchar(128)      not null,
	lastName      varchar(128)      not null,
	gender        varchar(128)      not null,
	birthday        datetime
);

LOAD DATA LOCAL INFILE '../../d/easycar-clients.csv'
INTO TABLE easycar_clients
FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(firstName,lastName,gender,birthday)
