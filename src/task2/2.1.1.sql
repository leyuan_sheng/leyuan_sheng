# Create table car with columns: vin, model_id, color, year, rate.
create table car(
	vin     	varchar(128)      	not null,
	model_id     	int(16)		      	not null,
	color         	varchar(128) 		not null,
	year		datetime,
	rate  float(8)
);
