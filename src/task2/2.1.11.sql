# Create a foreign key between tables car and model to provide better consistency.
ALTER TABLE car
ADD FOREIGN KEY (model_id) REFERENCES model(model_id);
