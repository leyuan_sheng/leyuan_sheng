#!/bin/bash
echo "1.1.5. Use split to divide l1.log by 4 files. First create folder d/log/l1 with mkdir. and then use split to put parts of the file into it."
mkdir -p ../../d/log/l1
split -l 50 ../../d/l1.log ../../d/log/l1/
