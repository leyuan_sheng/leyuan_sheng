#!/bin/bash
echo "1.2.1. Use jq to extract relation graph from friends.json, save it in dot-format and visualize."
jq -c '.friends[] |[.id,.name, .knows[0]]' ../../d/friends.json > t.csv
jq -c '.friends[] |[.id,.name, .knows[1]]' ../../d/friends.json > tt.csv
cat t.csv tt.csv >relation.csv
sed -i 's/\[//g;s/\]//g;s/,/;/g;s/"//g' relation.csv
awk -f ../csv-to-tree.awk relation.csv > relation.dot
dot -Tsvg -O relation.dot
eog relation.dot.svg
rm -rf t.csv tt.csv
